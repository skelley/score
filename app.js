const express = require('express')
const app = express();

app.use(express.json());

const scores = [
    {
      id : 1,
      name : "John",
      score : 850
    },
    {
       id : 2,
      name: "Bob",
      score: 800
    },
    {
        id : 3,
      name: "Mark",
      score: 900
    },
    {
      id : 4,
      name: "Marcy",
      score: 700
    },
    {
      id : 5,
      name: "Jake",
      score: 1000
      
    }
]



app.get('/', (req, res) => {
    res.send('<div id ="front"><strong>High Score Server</strong></div>');
});

app.get('/api/scores', (req, res) => {
    res.send(scores.sort())
})


app.post('/api/scores', (req, res) => {
    
    const scorePost = {
        id: scores.length + 1,
        name: req.body.name,
        score: req.body.score
    };
    scores.push(scorePost) ;
    res.send(scorePost);
})

//app.post('/api')

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`listening on port ${port}...`))

